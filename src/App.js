import React, {Component} from 'react';
import './App.css';
import Person from './Person/Person';
import styled  from 'styled-components';

const StyledButon = styled.button`
      
      background-color: green;
      color: white;
      font: inherit;
      padding: 3px;
      cursor: pointer;
      
      &:hover {
        color: ligthGreen;
      }
      
      `

class App extends Component {
  state = {
    persons:[
       {
         name: 'Hulk', age: '28',
         id: 0
       },
       {
         name: 'Angel', age: '12',
         id: 1
       },
       {
         name: 'Firefox', age: '23',
         id: 2
       }
     ],
     showPersons: false
   }
  switchName = (newName) => {
    this.setState( {
      persons:[
        {
          name: newName, age: '28'
        },
        {
          name: 'Angel', age: '12'
        },
        {
          name: 'Firefox', age: '23'
        }
      ] 
    })
  }
  switchNameHandler =  (event,id) => {
    let person  = this.state.persons.find( (person) => {
      return person.id === id
    })
    console.log(event.target.value)
    person.name = event.target.value
    let persons = [...this.state.persons]
    persons[id] = person
    console.log('persons', persons)
    this.setState( {
      persons : persons
    })
  }
  removePerson = (index) => {
   
    
    let persons = [...this.state.persons]
    persons.splice(index,1)
    this.setState({
      persons: persons
    })
  }
  showPersonsHandler = () =>{
    let doesShowPersons = this.state.showPersons
    this.setState({
      showPersons: !doesShowPersons
    })
  }
  
  render(){
   
    let persons = null
    if( this.state.showPersons){
      persons = (
        this.state.persons.map( (person,index) =>{
          return <Person clickEv={() => this.removePerson(index)} swithHandlerName={ (event) => this.switchNameHandler(event, person.id)} key={person.id} name={person.name} age={person.age} />
        })

      );


    }
    const classes = []
    if( this.state.persons.length  <= 2 ) { classes.push('red')}
    if( this.state.persons.length <= 1) { classes.push('bold')}

    return (
      <div className="App">
       <h1>
         React App
       </h1>
       <p className={classes.join(' ')}> This is my first react</p>
       <StyledButon onClick={this.showPersonsHandler}>
        Change Name
       </StyledButon>
        {persons}
      </div>
    );
  }
  
}

export default  App;
